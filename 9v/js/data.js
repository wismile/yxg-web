var db={
  page:{
    title:'9Video',
    api:'http://m.yxgapp.com/d/mooc/pms/',
    baseURI:'http://localhost:8000'
  },
  videos:[
    {
      id:39311,
      file:'video/1503651070827_1771.mp4',
      srt:'video/ext_39311.vtt',
      dTitle:'Beef Rice Bowl',
      sTitle:'电饭锅超嫩牛肉饭',
      image:'video/1.png'
    },
    {
      id:39310,
      file:'video/1503651145341_3.mp4',
      srt:'video/ext_39310.vtt',
      dTitle:'Fried chicken with beer',
      sTitle:'炸鸡配啤酒',
      image:'video/2.png'
    },
    {
      id:39309,
      file:'video/1503651163860_4550.mp4',
      srt:'video/ext_39309.vtt',
      dTitle:'Beef Rice Bowl',
      sTitle:'香菇鸡腿饭',
      image:'video/3.png'
    },
    {
      id:39308,
      file:'video/1503651391513_6595.mp4',
      srt:'video/ext_39308.vtt',
      dTitle:'Spicy Pig Trotters',
      sTitle:'香酥猪蹄',
      image:'video/4.png'
    },
  ]
}

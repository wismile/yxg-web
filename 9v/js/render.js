String.prototype.GetValue= function(para) {
  var reg = new RegExp("(^|&)"+ para +"=([^&]*)(&|$)");
  var r = this.substr(this.indexOf("\?")+1).match(reg);
  if (r!=null) return unescape(r[2]); return null;
}

function renderVideoList(){
  var model = $('<div class="col-md-3 col-xs-6 videoCard"><a href="#"><img src="video/1.png"><h4>Beef Rice Bowl<br><small>红烧牛肉饭</small></h4></a></div>');
  var videoCon = $('#videoCon');
  videoCon.empty();
  for(var i=0;i<db.videos.length;i++){
    var m=model.clone();
    m.find('a').attr('href','player.html?id='+db.videos[i].id);
    m.find('img').attr('src',db.videos[i].image);
    m.find('h4').html(db.videos[i].dTitle + '<br><small>'+ db.videos[i].sTitle +'</small>');
    videoCon.append(m);
  }
}

function getTime(mss){
  var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = parseInt((mss % (1000 * 60)) / 1000);
  var ms = mss % 1000 ;
  ms = ms.toString();
  while (ms.length <3 ) {
    ms = '0'+ms;
  }
  return (hours<10?'0'+hours:hours) + ":" + (minutes<10?'0'+minutes:minutes) + ":" + (seconds<10?'0'+seconds:seconds)+ "." + ms;
}

function subtime2ms(time){
  var t = time.split(':');
  ms = t[0]*60*60*1000 + t[1]*60*1000 + t[2]*1000;
  return(ms)
}

function getUserId(){
  var userId = $.cookie('uid');
  if(!userId){
    userId = newGuid()
  }
  $.cookie('uid',userId,{expires:365});
  return userId;
}

function newGuid(){
  var guid = "";
  for (var i = 1; i <= 32; i++){
    var n = Math.floor(Math.random()*16.0).toString(16);
    guid +=   n;
    if((i==8)||(i==12)||(i==16)||(i==20))
      guid += "-";
  }
  return guid;
}

function srt2array(srtString){
  var srtArray = srtString.split('\n');
  for(var i=0;i<srtArray.length;i++){
    console.log(srtArray[i])
  }
}

function saveComment(userIdentity,videoId,subIndex,mark){
  var data = {
    userIdentity:userIdentity,
    videoName:videoId,
    subIndex:subIndex,
    mark:mark,
    action:'save'
  };
  var url = 'http://m.yxgapp.com/d/mooc/pms/videoMarks.json';
  $.get(url,data);
}

function listComment(userIdentity,videoId){
  var data = {
    userIdentity:userIdentity,
    videoName:videoId,
    action:'list'
  };
  var url = 'http://m.yxgapp.com/d/mooc/pms/videoMarks.json';
  $.get(url,data,function(d){
    if(d.result.result){
      var items = $('div.item');
      for(var i=0;i<d.list.length;i++){
        $(items[d.list[i].subIndex-1]).append('<p class="comment">Comment : <span>'+ d.list[i].mark +'</span></p>');
      }
    }
  },'json');
}

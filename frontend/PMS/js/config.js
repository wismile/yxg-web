var config = {
  tableConfig : {
    'language': {
      'emptyTable': '没有数据',
      'loadingRecords': '加载中...',
      'processing': '查询中...',
      'search': '检索:',
      'lengthMenu': '每页 _MENU_ 条',
      'zeroRecords': '没有数据',
      'paginate': {
        'first':      '首页',
        'last':       '尾页',
        'next':       '下一页',
        'previous':   '上一页'
      },
      'info': '第 _PAGE_ 页 / 共 _PAGES_ 页 | 总记录数 _TOTAL_',
      'infoEmpty': '没有数据',
      'infoFiltered': '(过滤前共 _MAX_ 条)'
    },
    "bStateSave": true,
    "lengthMenu": [[ 50, 100, -1 ],[ 50, 100, '所有' ]],
    "pageLength": 50,
  },
  daterangeConfig : {
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: '2017-05-01',
		maxDate: moment(),
		dateLimit: { days: 60 },
		showDropdowns: false,
		showWeekNumbers: false,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		opens: 'left',
		buttonClasses: ['btn btn-default'],
		applyClass: 'btn-small btn-primary',
		cancelClass: 'btn-small',
		format: 'YYYY-MM-DD',
		separator: ' - ',
		locale: {
			applyLabel: '确定',
			cancelLabel: '取消',
			fromLabel: '从',
			toLabel: '到',
			customRangeLabel: '自定义区间',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		}
  },
  app:{
    title : '译学馆短视频国际化平台',
    status :[
      {value:0,text:'未开始'},
      {value:1,text:'创建任务'},
      {value:2,text:'抓取中'},
      {value:3,text:'8待认领'},
      {value:4,text:'7翻译中'},
      {value:5,text:'1字幕待确认'},
      {value:6,text:'未上线'},
      {value:7,text:'6审核中'},
      {value:8,text:'字幕已确认'},
      {value:10,text:'等待抓取'},
      {value:11,text:'4特效制作'},
      {value:12,text:'5特效关闭'},
      {value:13,text:'2特效待确认'},
      {value:14,text:'3水印合成'},
      {value:89,text:'归档'},
      {value:99,text:'删除'},
      //{value:15,text:'特效完成'},
    ],
    refreshInterval : 60000,
  }
}

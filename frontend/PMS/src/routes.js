const login = resolve => require(['./components/login.vue'], resolve)
const loginEmail = resolve => require(['./components/loginEmail.vue'], resolve)
const index = resolve => require(['./components/index.vue'], resolve)
const noright = resolve => require(['./components/noright.vue'], resolve)
const nomodel = resolve => require(['./components/nomodel.vue'], resolve)
const dashboard = resolve => require(['./components/dashboard.vue'], resolve)
const util = resolve => require(['./components/util.vue'], resolve)
const uploadVideo = resolve => require(['./components/uploadVideo.vue'], resolve)
const cooperatorManage = resolve => require(['./components/cooperator/cooperatorManage.vue'], resolve)
const taskManage = resolve => require(['./components/task/taskManage.vue'], resolve)
const effectkManage = resolve => require(['./components/effect/effectManage.vue'], resolve)
const allEffectTask = resolve => require(['./components/effect/allTask.vue'], resolve)
const archiveEffectTask = resolve => require(['./components/effect/archiveTask.vue'], resolve)
const watermark = resolve => require(['./components/effect/watermark.vue'], resolve)
const exportPath = resolve => require(['./components/export/path.vue'], resolve)
const transTemplate = resolve => require(['./components/export/transTemplate.vue'], resolve)
const exportTasks = resolve => require(['./components/export/task.vue'], resolve)
const exTaskList = resolve => require(['./components/task/exTaskList.vue'], resolve)
const exTaskForm = resolve => require(['./components/task/exTaskForm.vue'], resolve)
const feedbackList = resolve => require(['./components/task/feedbackList.vue'], resolve)
const taskArchive = resolve => require(['./components/task/taskArchive.vue'], resolve)
const user = resolve => require(['./components/system/user.vue'], resolve)
const material = resolve => require(['./components/cooperator/material.vue'], resolve)
const merge = resolve => require(['./components/task/mergeTask.vue'], resolve)

export default (function (){
let config = [
    {name:"root", path: '/', redirect: '/login' },
    //{name:"login", path: '/login',components: {'main' : login}},
    {name:"login", path: '/login',components: {'main' : loginEmail}},
    {name:"index", path: '/index',components: {'main' : index},
        children:[
            {name:"noright",path:"noright",components:{'page':noright}},
            {name:"dashboard",path:"dashboard",components:{'page':dashboard}}
        ]
    },
    {name:"cooperator", path: '/cooperator',components: {'main' : index},
        children:[
            {name:"manage",path:"manage",components:{'page':cooperatorManage}},
            {name:"upload",path:"upload",components:{'page':uploadVideo}},
            {name:"material",path:"material",components:{'page':material}}
        ]
    },
    {name:"task", path: '/task',components: {'main' : index},
        children:[
            {name:"manage",path:"manage",components:{'page':taskManage},
              children:[
                {name:"exTaskList",path:"exList/:taskId",components:{'exTask':exTaskList}},
                {name:"exTaskForm",path:"exForm/:taskId/:exTaskId",components:{'exTask':exTaskForm}},
                {name:"feedbackList",path:"feedback/:taskId",components:{'exTask':feedbackList}},
                {name:"mergeTask",path:"merge/:taskId",components:{'exTask':merge}}
              ]
            },
            {name:"archive",path:"archive",components:{'page':taskArchive}},
            {name:"upload",path:"upload",components:{'page':uploadVideo}}
        ]
    },
    {name:"effect", path: '/effect',components: {'main' : index},
        children:[
            {name:"manage",path:"manage",components:{'page':effectkManage}},
            {name:"archiveEffectTask",path:"archiveTask",components:{'page':archiveEffectTask}},
            {name:"allEffectTask",path:"allTask",components:{'page':allEffectTask},
            children:[
              {name:"feedbackList",path:"feedback/:taskId",components:{'exTask':feedbackList}},
              {name:"watermark",path:"watermark/:taskId",components:{'exTask':watermark}},
              {name:"mergeTask",path:"merge/:taskId",components:{'exTask':merge}}
            ]
          }
        ]
    },
    {name:"export", path: '/export',components: {'main' : index},
        children:[
            {name:"exportPath",path:"path",components:{'page':exportPath}},
            {name:"exportTemplate",path:"script",components:{'page':transTemplate}},
            {name:"exportTask",path:"task",components:{'page':exportTasks}}
        ]
    },
    {name:"system", path: '/system',components: {'main' : index},
        children:[
            {name:"user",path:"user",components:{'page':user}}
        ]
    }
];
  return config;
})()

var iOSAndJSAndAndroidCompatible = {};
iOSAndJSAndAndroidCompatible.checkBrowserForIOS = function () {
  var browser = {
    versions: function () {
      var u = navigator.userAgent, app = navigator.appVersion;
      return {         //移动终端浏览器版本信息
        trident: u.indexOf('Trident') > -1, //IE内核
        presto: u.indexOf('Presto') > -1, //opera内核
        webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
        iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
        iPad: u.indexOf('iPad') > -1, //是否iPad
        webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
      };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
  };
  if (browser.versions.mobile) {//判断是否是移动设备打开。browser代码在下面
    var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
      //在微信中打开
      return 'wx'
    }
    if (ua.match(/WeiBo/i) == "weibo") {
      //在新浪微博客户端打开
    }
    if (ua.match(/QQ/i) == "qq") {
      //在QQ空间打开
    }
    if (browser.versions.ios) {
      //是否在IOS浏览器打开
      return 'ios'
    }
    if (browser.versions.android) {
      //是否在安卓浏览器打开
    }
  } else {
    //否则就是PC浏览器打开
  }
};

iOSAndJSAndAndroidCompatible.setupWebViewJavascriptBridge = function(callback) {

  if (window.WebViewJavascriptBridge) {
    return callback(WebViewJavascriptBridge);
  }
  if (window.WVJBCallbacks) {
    return window.WVJBCallbacks.push(callback);
  }
  window.WVJBCallbacks = [callback];
  var WVJBIframe = document.createElement('iframe');
  WVJBIframe.style.display = 'none';
  WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
  document.documentElement.appendChild(WVJBIframe);
  setTimeout(function () {
    document.documentElement.removeChild(WVJBIframe)
  }, 0)

};

iOSAndJSAndAndroidCompatible.handleJSMethodForIOS = function (bridge) {

  var status = 0;

  var checkStatus = function(){
    if(status>=2){
      runPage();
    }
  };

  bridge.callHandler('getUsername', {}, function (responseData) {
    window.Native.getUsername = function () {
      return responseData
    };
    status++;
    checkStatus();
  });

  bridge.callHandler('getToken', {}, function (responseData) {
    window.Native.getToken = function () {
      return responseData
    };
    status++;
    checkStatus();
  });

  window.Native.setTitle = function (title) {
    bridge.callHandler('setTitle', {'title': title}, function (responseData) {
    });
  };

  window.Native.closePage = function () {
    bridge.callHandler('closePage', {}, function (responseData) {
    });
  };

  window.Native.showToast = function (message) {
    bridge.callHandler('showToast', {'message': message}, function (responseData) {
    });
  };

  window.Native.getSubtitleCheckMark = function(videoId,callback){
    //alert(videoId);
    bridge.callHandler('getSubtitleCheckMark', {
      'videoId': videoId
    }, function (responseData) {
      callback(responseData);
    });

  };

  window.Native.subtitleCheckResult = function(result){
    return bridge.callHandler('subtitleCheckResult', {
      'result': result
    }, function (responseData) {
      return responseData
    });
  };

  window.Native.startPlayVideo = function (courseId, videoId) {
    bridge.callHandler('startPlayVideo', {
      'courseId': courseId,
      'videoId': videoId
    }, function (responseData) {
    });
  };

  window.Native.startSubtitleHistory = function (courseId, videoId) {
    bridge.callHandler('startSubtitleHistory', {
      'courseId': courseId,
      'videoId': videoId
    }, function (responseData) {
    });
  };

  window.Native.setBackEnabled = function (enable) {
    bridge.callHandler('setBackEnabled', {'enable': enable}, function (responseData) {});
  };

  window.Native.setCloseConfirm = function (confirm, title, message) {
    bridge.callHandler('setCloseConfirm', {
      'confirm': confirm,
      'title': title,
      'message': message
    }, function (responseData) {});
  };

  window.Native.examResultExamTranslatorPage = function (score, passed) {
    bridge.callHandler('examResultExamTranslatorPage', {
      'score': score,
      'passed': passed
    }, function (responseData) {});
  };
};

$(function(){
  if (iOSAndJSAndAndroidCompatible.checkBrowserForIOS() == 'ios') {
    window.Native = {};
    iOSAndJSAndAndroidCompatible.setupWebViewJavascriptBridge(iOSAndJSAndAndroidCompatible.handleJSMethodForIOS);
  }else{
    runPage();
  }
});

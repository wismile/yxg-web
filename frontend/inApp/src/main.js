import "babel-polyfill"
import Vue from 'vue'
import VueRouter from 'vue-router'
//import VeeValidate from 'vee-validate'

import entry from './components/entry.vue'
import routes from './routes.js'

Vue.use(VueRouter)
//Vue.use(VeeValidate)



const router = new VueRouter({
  mode:'hash',
  routes:routes
});

// router.beforeEach((to, from, next) => {
//
//   console.info("beforeEach::::::::::");
//   console.info("to name::::::" + to.name);
//   console.info("from name::::::" + from.name);
//
//
//   var t = setInterval(function(){
//     if(window.Native &&  window.Native.getSubscribeChannelIds){
//       clearInterval(t);
//       console.log('sleep')
//       next();
//     }else{
//       //next();
//     }
//   },100);
//   //setTimeout(next,1000);
//
// })



const app = new Vue({
    router,
    render: h => h(entry)
}).$mount('#app')

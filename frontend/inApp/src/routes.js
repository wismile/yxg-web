const error = resolve => require(['./components/noright.vue'], resolve)
const checkTask = resolve => require(['./components/checkTask.vue'], resolve)
const opTask = resolve => require(['./components/opTask.vue'], resolve)
const levelRight = resolve => require(['./components/levelRight.vue'], resolve)
const submitRight = resolve => require(['./components/submitRight.vue'], resolve)
const translatorExam = resolve => require(['./components/translatorExam.vue'], resolve)
const myAccount = resolve => require(['./components/myAccount.vue'], resolve)

export default (function (){
  let config = [
      {name:"root", path: '/', redirect: '/error' },
      {name:"error", path: '/error',components: {'main' : error}},
      {name:"checkTask", path: '/checkTask',components: {'main' : checkTask}},
      {name:"opTask", path: '/opTask',components: {'main' : opTask}},
      {name:"levelRight", path: '/levelRight',components: {'main' : levelRight}},
      {name:"submitRight", path: '/submitRight',components: {'main' : submitRight}},
      {name:"translatorExam", path: '/translatorExam',components: {'main' : translatorExam}},
      {name:"myAccount", path: '/myAccount',components: {'main' : myAccount}},
  ];
  return config;
})()

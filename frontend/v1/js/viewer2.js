var ani=false;
var fristSeries='';
$(function(){
    buildCanvas();
    getSeries(); 
    $("#leftPane").on('click','a.serbtn',function(){
        if(!ani){
            ani=true;
            var _this = $(this);
            _h = $(this).next('div').height();
            if(_this.hasClass('act')){
               _this.parent('div').animate({height:'200px'},function(){
                   _this.removeClass('act');
                   ani=false;
               })
            }else{
                _this.parent('div').animate({height:(_h) +'px'},function(){
                   _this.addClass('act');
                   _this.parent('div').css('height','auto');
                   ani=false; 
                   $("#leftPane img[src='images/load.gif']").lazyload({placeholder : "images/load.gif",effect: "fadeIn"});
               })
            }
            
        }  
    });
    $("#leftPane").scroll(function(){$("#leftPane img[src='images/load.gif']").lazyload({placeholder : "images/load.gif",effect: "fadeIn"});});
    
    $("#leftPane").on('click','img',function(){
        var _this = $(this);
        //$("#leftPane img").removeClass('act');
        //_this.addClass('act');
        drawimage(_this.data('seruid'),_this.data('index')-1);
    });
    $("#page").on('click','td',function(){
        $("#page td").removeClass('act');
        $(this).addClass('act');
    });
    $("input.slider").slider();
    $('#page').on('mousewheel','td.act',function(event){
        //console.log(event.deltaX, event.deltaY, event.deltaFactor);
        if(event.deltaY>0){
            drawimage($(this).data('series'),$(this).data('index')-1);
        }else{
            drawimage($(this).data('series'),$(this).data('index')+1);
        }
    });
    $(document).keyup(function(e){
        var _this = $('#page td.act');
        if(e.keyCode == 38){
            drawimage(_this.data('series'),_this.data('index')-1);
        }
        if(e.keyCode == 40){
            drawimage(_this.data('series'),_this.data('index')+1);
        }
        if(e.keyCode == 37){
            var _tds = $("#page td");
            var _count = _tds.length;
            if(_count>1){
                for(var i=0;i<_count;i++){
                    if($(_tds[i]).hasClass('act')){
                        if(i>0) $(_tds[i-1]).trigger('click');
                        break;
                    }
                }
            }
        }
        if(e.keyCode == 39){
            var _tds = $("#page td");
            var _count = _tds.length;
            if(_count>1){
                for(var i=0;i<_count;i++){
                    if($(_tds[i]).hasClass('act')){
                        if(i<_count) $(_tds[i+1]).trigger('click');
                        break;
                    }
                }
            }
        }
    });
});

function drawimage(series,index){

    var _td = $("#page td.act");
    var _canvas = _td.find("canvas");
    var _h = _canvas.height();
    var _w = _canvas.width();
    var data = JSON.parse(sessionStorage[series]);
    var total = data.length;
    if(index < 0) index = total -1;
    if(index == total) index = 0;
    var ih = data[index].nativeRows;
    var iw = data[index].nativeColumns;
    var wl = data[index].windowCenter;
    var ww = data[index].windowWidth;
    var thick = data[index].sliceThickness;
    var loc = data[index].sliceLocation;
    var scaleh = parseInt(_h / ih *100);
    var scalew = parseInt(_w / iw *100);
    if(scaleh > scalew) scaleh = scalew;
    
    var imgOrient = data[index].imageOrientation.split("\\");
    
    _td.find('div.imgOriRight').html(imgOrient[0]);
    _td.find('div.imgOriBottom').html(imgOrient[1]);
    _td.find('div.imgOriLeft').html(getOppositeOrientation(imgOrient[0]));
    _td.find('div.imgOriTop').html(getOppositeOrientation(imgOrient[1]));
    
    _td.find('div.patInfo').html(patName +'<br>' + patID);
    _td.find('div.seriesInfo').html(studyDescription +'<br>' + studyDate + '<br>' + getSeriesDesc(series));
    _td.find('div.imgInfo').html('Images: '+ (index+1) +' / '+ total +'<br>' + 'WL: '+ wl +' / WW: '+ww +'<br>' + 'Thick: '+ Math.round(thick*100)/100 +' / LOC: '+Math.round(loc*100)/100);
    _td.find('div.viewInfo').html('Image size: '+ iw + ' x ' + ih +'<br>' + 'View size: '+ _w + ' x ' + _h +'<br>'+'Zoom: ' + scaleh + '%');
    _td.data("series",series).data("index",index) ;
    _canvas.clearCanvas();
    _canvas.drawImage({
        source: 'Image.do?serverURL='+serverURL+'&study='+studyUID+'&series='+series+'&object='+data[index].SopUID+'&rows='+data[index].nativeRows,
        x:_w/2,
        y:_h/2,
        scale:scaleh/100
    }); 
    
    $("#leftPane img[data-seruid='"+series+"'][data-index='"+ (index+1) +"']").addClass('act').siblings().removeClass('act');
}

function getSeriesDesc(series){
    var obj = JSON.parse(sessionStorage[studyUID]);
    for(var i=0;i<obj.length;i++){
        if(obj[i].seriesUID == series){
            return(obj[i].seriesDesc);
        }
    }
}

function getOppositeOrientation(str) {
    var strTmp = '';
    for(i=0; i<str.length; i++) {
        switch(str.charAt(i)) {
            case 'P':
                strTmp += 'A';
                break;
            case 'A':
                strTmp += 'P';
                break;
            case 'I':
                strTmp += 'S';
                break;
            case 'S':
                strTmp += 'I';
                break;
            case 'F':
                strTmp += 'H';
                break;
            case 'H':
                strTmp += 'F';
                break;
            case 'L':
                strTmp += 'R';
                break;
            case 'R':
                strTmp += 'L';
                break;
        }
    }

    return strTmp;
}

function buildCanvas(r,c){
    if(r==undefined || c==undefined) {
        r=1; c=1;
    }
    var _con=$('#page');
    _con.empty();
    var _h = _con.height()-r;
    var _w = _con.width();
    var t=document.createElement('table');
    for(var i=0; i<r; i++ ){
        var tr = document.createElement('tr');
        for(var j=0; j<c; j++){
            var _td = document.createElement('td');
            _td.appendChild(document.createElement('canvas'));
            $(_td).append('<div class="imgOriTop"></div><div class="imgOriBottom"></div><div class="imgOriLeft"></div><div class="imgOriRight"></div><div class="patInfo"></div><div class="seriesInfo"></div><div class="imgInfo"></div><div class="viewInfo"></div>');
            $(tr).append(_td);
        }
        $(t).append(tr);
    }
    $(t).find('canvas').attr('height',(_h/r - 6)+'px').attr('width',(_w/c -2)+'px');
    $(t).find('td:eq(0)').addClass('act');
    $('#page').append(t);
}

function getSeries() {
	$.post("Series.do", {
		"patientID" : patID,
		"studyUID" : studyUID,
		"dcmURL" : dcmURL
	}, function(data) {
        sessionStorage[studyUID] = JSON.stringify(data);
        fristSeries = data[0].seriesUID;
		if(serverURL != 'C-MOVE' && serverURL != 'C-GET') {
			$.each(data, function(i, series) {
                var _div = document.createElement('div');
                _div.className = 'ser';
                _div.id = series.seriesUID.replace(/\./g,'_');
                $("#leftPane").append(_div);
                getInstances(patID, studyUID, series);
			});
		}
	}, "json");
}

function getInstances(patId, studyUID, series) {
    console.log('===========start' +series.seriesDesc + '========' + Date.parse(new Date()));
	$.post("Instance.do", {
		"patientId" : patId,
		"studyUID" : studyUID,
		"seriesUID" : series.seriesUID,
		"dcmURL" : dcmURL,
		"serverURL" : serverURL
	}, function(data) {
        sessionStorage[series.seriesUID] = JSON.stringify(data);
        var model = $('<a href="#" class="serbtn">序列:'+ series.seriesDesc +' 图片总数:'+ series.totalInstances +'</a><div class="img_con"></div>');
        $.each(data, function(i, instance) {
			model.filter("div.img_con").append('<img data-index="'+ instance.InstanceNo +'" data-serUID="'+series.seriesUID+'" data-original="Image.do?serverURL='+serverURL+'&study='+studyUID+'&series='+series.seriesUID+'&object='+instance.SopUID+'&rows='+instance.nativeRows+'" src="images/load.gif">');
            //model.filter("div.img_con").append('<img data-index="'+ instance.InstanceNo +'" data-serUID="'+series.seriesUID+'" src="Image.do?serverURL='+serverURL+'&study='+studyUID+'&series='+series.seriesUID+'&object='+instance.SopUID+'&rows='+instance.nativeRows+'">');
		});
        $("#"+series.seriesUID.replace(/\./g,'_')).append(model);
        console.log('===========finish' +series.seriesDesc + '========' + Date.parse(new Date()));
        if(fristSeries == series.seriesUID) drawimage(series.seriesUID,0);
        setTimeout(function(){
            $("#leftPane img[src='images/load.gif']").lazyload({placeholder : "images/load.gif",effect: "fadeIn"});
        },200);
	}, "json");
}

function triggerPlay(){
}
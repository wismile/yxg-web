import "babel-polyfill"
import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import VueHtml5Editor from 'vue-html5-editor'

import entry from './components/entry.vue'
import routes from './routes.js'

Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(VueHtml5Editor)

const router = new VueRouter({
  mode:'hash',
  routes:routes
});

/*router.beforeEach((to, from, next) => {

  console.info("beforeEach::::::::::");
  console.info("to name::::::" + to.name);
  console.info("from name::::::" + from.name);

  //判断是否保存编辑内容
  if(GQCMS.isModi === true && !(to.name == "searchResultCollection" || to.name == "searchResultVideos" )){
    var confirm = window.confirm("您编辑的内容还没有保存，点确定将失去所有编辑信息");
    if(confirm){
      GQCMS.isModi = false;
      next();
    }else{
      return;
    }
  }else{
    next();
}
})*/

/*var main = new Vue({
  el: '#app',
  data:{a:123},
  router,
  render: h => h(entry)
})*/
const app = new Vue({
    router,
    render: h => h(entry)
}).$mount('#app')

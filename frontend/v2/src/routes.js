const login = resolve => require(['./components/login.vue'], resolve)
const index = resolve => require(['./components/index.vue'], resolve)
const submitVideo = resolve => require(['./components/submitVideo.vue'], resolve)
const noright = resolve => require(['./components/noright.vue'], resolve)
const nomodel = resolve => require(['./components/nomodel.vue'], resolve)
const pointJournal = resolve => require(['./components/teamPointJournal.vue'], resolve)
const manageStaff = resolve => require(['./components/teamManageStaff.vue'], resolve)
const dashboard = resolve => require(['./components/dashboard.vue'], resolve)
const myTask = resolve => require(['./components/myTask.vue'], resolve)
const submitChannel = resolve => require(['./components/submitChannel.vue'], resolve)
const myTags = resolve => require(['./components/myTags.vue'], resolve)
const modifyVideoTag = resolve => require(['./components/modifyVideoTag.vue'], resolve)
const sysTags = resolve => require(['./components/sysTags.vue'], resolve)
const uploadVideo = resolve => require(['./components/uploadVideo.vue'], resolve)
const subEffectTask = resolve => require(['./components/subEffectTask.vue'], resolve)
const allEffectTask = resolve => require(['./components/allEffectTask.vue'], resolve)
const feedbackList = resolve => require(['./components/feedbackList.vue'], resolve)
const videoTag = resolve => require(['./components/manageVideoTag.vue'], resolve)
const myTaskFinish = resolve => require(['./components/myTaskFinish.vue'], resolve)

export default (function (){
  /*let config = [
    {name:"root", path: '/', redirect: '/apphome' },
    {name:"apphome",  path: '/apphome', components: {'mainbody' : apphome},
      children:[
      {name:"dashboard",  path: 'dashboard', components: {'rightBody':dashboard}},
      {name:"applist",  path: 'show', components: {'rightBody':rightBody},
        children:[
        {name:"searchTools",  path: 'detail', components: {'searchTools':searchTools, 'videoListForReco': videoListForReco },
          children:[
          { name:"searchResultCollection",  path: 'search', components: {'searchResultCollection':searchResultCollection} },
          { name:"searchResultVideos",  path: 'searchVideos', components: {'searchResultCollection':searchResultVideos} }
          ]
        }
      ],
      }
      ]
    },
    {name:"applist",  path: '/applist', components: {'mainbody':applist, "nav": nav} }
];*/
let config = [
    {name:"root", path: '/', redirect: '/login' },
    {name:"login", path: '/login',components: {'main' : login}},
    {name:"index", path: '/index',components: {'main' : index},
        children:[
            {name:"noright",path:"noright",components:{'page':noright}},
            {name:"dashboard",path:"dashboard",components:{'page':dashboard}}
        ]
    },
    {name:"team", path: '/team',components: {'main' : index},
        children:[
            {name:"manageStaff",path:"manageStaff",components:{'page':manageStaff}},
            {name:"pointJournal",path:"pointjournal",components:{'page':pointJournal}}
        ]
    },
    {name:"translator", path: '/translator',components: {'main' : index},
        children:[
            {name:"myTask",path:"myTask",components:{'page':myTask}},
            {name:"myTaskFinish",path:"myTaskFinish",components:{'page':myTaskFinish}},
            {name:"claimTask",path:"claimTask",components:{'page':nomodel}}
        ]
    },
    {name:"recommend", path: '/reco',components: {'main' : index},
        children:[
            {name:"submitVideo",path:"submitVideo",components:{'page':submitVideo}},
            {name:"uploadVideo",path:"uploadVideo",components:{'page':uploadVideo}},
            {name:"submitChannel",path:"submitChannel",components:{'page':submitChannel}},
            {name:"myTags",path:"myTags",components:{'page':myTags}}
        ]
    },
    {name:"manager", path: '/sys',components: {'main' : index},
        children:[
            {name:"videoTag",path:"videoTag",components:{'page':modifyVideoTag}},
            {name:"tags",path:"tags",components:{'page':sysTags}}
        ]
    },
    {name:"cms", path: '/cms',components: {'main' : index},
        children:[
            {name:"videoTag",path:"videoTag",components:{'page':videoTag}},
        ]
    },
    {name:"subEffect", path: '/subEffect',components: {'main' : index},
        children:[
            {name:"task",path:"task",components:{'page':subEffectTask},
              children:[
                {name:"feedbackList",path:"feedback/:taskId",components:{'exTask':feedbackList}},
              ]
            },
            {name:"myTask",path:"myTask",components:{'page':subEffectTask}},
            {name:"allTask",path:"allTask",components:{'page':allEffectTask}}
        ]
    }
];
  return config;
})()

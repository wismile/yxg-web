String.prototype.GetValue= function(para) {
    var reg = new RegExp("(^|&)"+ para +"=([^&]*)(&|$)");
    var r = this.substr(this.indexOf("\?")+1).match(reg);
    if (r!=null) return unescape(r[2]); return null;
};
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?77480e9365b9380722fe7897e6606066";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(hm, s);
})();

var page = {
    checkUA : function(){
        var ua = navigator.userAgent.toLowerCase();
        if(/micromessenger/.test(ua)){
            return('wx')
        }else if(/iphone|ipad|ipod/.test(ua)) {
            return('ios')
        }else if(/android/.test(ua)){
            return('and')
        }else if(/windows phone/.test(ua)){
            return('wm')
        }else if(/baiduspider/.test(ua)){
            return('baidu')
        }else if(/googlebot/.test(ua)){
            return('google')
        }else if(/yahoo/.test(ua)){
            return('yahoo')
        }else if(/iaskspider/.test(ua)){
            return('iask')
        }else if(/sogou/.test(ua)){
            return('sogou')
        }else if(/youdaobot|yodaobot/.test(ua)){
            return('yodao')
        }else if(/bingbot/.test(ua)){
            return('bing')
        }else if(/sosospider/.test(ua)){
            return('soso')
        }else if(/360spider/.test(ua)){
            return('360')
        }else{
            return('pc')
        }
    },
    getWxSign : function(url,callback){
        var uri = 'http://m.yxgapp.com/d/mooc/WXJSSDKHelper.json';
        var data = {localURL:url};
        $.get(uri,data,function(data){
            if(data.result.result){
                var wxconfig = {
                    appId: data.appId,
                    timestamp: data.timestamp,
                    nonceStr: data.nonceStr,
                    signature: data.signature,
                    jsApiList: []
                };
                callback(wxconfig)
            }else{
                callback(false);
            }
        },'json');
    },
    setEvents : function(d,o,v,b,u){
        var uri = 'http://ustat.yxgapp.com:8383/click';
        var data = {
            d:d,
            v:v,
            o:o,
            b:b,
            u:''
        }
        if(u != undefined) data.u=u;
        $.get(uri,data);
    },
    addGetApp : function(el){
        var bid = '';
        var u ='';
        if(window.uid != undefined) u = window.uid;
        if(window.vid != undefined) bid = window.vid;
        if(window.pid != undefined) bid = window.pid;
        var thiz = this;
        var model = $('<footer id ="getApp"><img src="/images/applogo.png"><div><p class="t">下载APP 体验更佳</p><div class="textList"></div></div><a href="#"><img src="/images/downbtn.png"></a><footer>');
        var textList = [
            '国内首个跨语言知识聚合平台',
            '数千译者日夜译制全球精选视频',
            '双语字幕切换 离线免流观看'
        ];
        var links = {
            wx : 'http://a.app.qq.com/o/simple.jsp?pkgname=com.huaer.mooc',
            ios : 'https://itunes.apple.com/cn/app/yi-xue-guan-zui-xin-zui-quan/id1019453226?mt=8' ,
            and : 'http://m.yxgapp.com/d/mooc/downloadApp?package=com.huaer.mooc'
        };
        switch(this.checkUA()){
            case 'wx':
                model.find('a').attr('href',links.wx);
                break;
            case 'ios':
                model.find('a').attr('href',links.ios);
                break;
            default:
                model.find('a').attr('href',links.and);
        }
        model.find('a').click(function(){
            thiz.setEvents(thiz.checkUA(),bid,'1.0.0','downApp',u);
            ga('send', 'event','getApp','getAppBar',bid);
            _hmt.push(['_trackEvent', 'getApp', 'getAppBar', 'click', bid]);
            if(window.co != undefined) {
                window.co.downApp=1;
                $.cookie('yxg_cookie',btoa(JSON.stringify(window.co)));
            }
        });
        var warper = $('<ul></ul>');
        for(var i=0; i<textList.length; i++){
            warper.append('<li>'+ textList[i] +'</li>');
        }
        model.find('div.textList').append(warper);
        $(el).append(model);
        setInterval(function(){
            warper.animate({marginTop:'-'+ warper.closest('div').height() +'px'}, 500, function(){
                warper.css({marginTop: "0px" }).find("li:first").appendTo(this);
            });
        },1500);
    },
    showWxShare : function(){
        var wxshare = '<div id="wx_share" style="position:fixed; width:100%; height:100%; bottom:0; background:rgba(0,0,0,.75) url(/static/images/share_wx.png) right top no-repeat; background-size:contain;"><a id="close" href="#">知道啦</a></div>';
        var thiz = this;
        if($('#wx_share').length == 0){
            $('body').append(wxshare).css('overflow','hidden');
            $('#wx_share').click(function(){
                thiz.hideWxShare();
            });
        }
    },
    hideWxShare : function(){
        $('#wx_share').remove();
        $('body').css('overflow','auto');
    }
};

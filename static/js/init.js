var hUnit = 0;
$(function(){
  initMenu();
  //initSlide();
  renderTranNum();
  $("#go_top").click(function(){
    $(window).scrollTop(0);
  });
  $(window).scroll(function(){
    if($(window).scrollTop()>0 && $(window).width()<=768 ){
      $("#go_top").show();
    }else{
      $("#go_top").hide();
    }
  });
  if($(window).width()<768){
    page.addGetApp('body');
  }
  // else{
  //   initVlink();
  // }
  setInterval(function(){
    $.get("http://m.yxgapp.com/d/mooc/GetTranslatorAndSentencesNumber.json",{},function(data){
        data=JSON.parse(data);
        billboard.change($("#tranNum"),data.sentencesNumber.toString(),data.translatorNumber);
    });
  },60000);
});
function setMenu(){
    var _width = $('#menucon').width();
    var activeItems = $('ul.nav li.active');
    var menu_width = activeItems.outerWidth();
    var prevMenuItems = activeItems.prevAll('li');
    var nextMenuItems = activeItems.nextAll('li');
    prevMenuItems.each(function(){
        menu_width += $(this).outerWidth();
    });
    if(menu_width > _width){
      $('#menucon').scrollLeft(menu_width - _width);
    }
    nextMenuItems.each(function(){
        menu_width += $(this).outerWidth();
    });
    $('#menucon ul').width(menu_width+10);
}
// function initSlide(){
//   if($('#slider').length > 0){
//     $('#slider').swipe({
//       swipeLeft:function(event, direction, distance, duration, fingerCount) {
//         $('#slider').carousel('next');
//       },
//       swipeRight:function(event, direction, distance, duration, fingerCount) {
//         $('#slider').carousel('prev');
//       },
//       threshold:40
//     });
//   }
// }
function initMenu(){
  if($('#menucon').length > 0){
    setMenu();
    $(window).resize(function(){
      setMenu()
    });
    $("div.menubtn").click(function(){
      if($('ul.popup:visible').length == 0){
        $('ul.popup').show();
      }else{
        $('ul.popup').hide();
      }
    });
  }
}
// function initVlink(){
//   $('a.vlink').each(function(){
//     $(this).attr('href','http://video.yxgapp.com/vplay.html?videoId=' + $(this).data('vid'));
//   });
// }

var billboard = {
    init:function(e,num,translator){
        var i=0;
        var model = $('<div><span class="hidden-xs">由</span><strong></strong>名译者贡献</div><div class="tranNum"></div><div class="hidden-xs">句</div>');
        var ultemp = '<div><ul><li>0</li><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>0</li></ul></div>';
        e.empty();
        model.find('strong').html(translator);
        while(i<num.length){
            $(model[1]).append(ultemp);
            i++;
        }
        e.html(model);
        hUnit = $('#tranNum .tranNum div').outerHeight();
        for(i=num.length; i >0 ;i--){
            this.scroll(e.find('div.tranNum'),i-1,parseInt(num.substr(i-1,1)));
        }
    },
    scroll:function(e,p,num){
        var _e = e.find("div:eq("+ p +")");
        if(_e.length==0){
            _e = e.prepend('<div><ul><li>0</li><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>0</li></ul></div>').find("div:eq("+ p +")");
        }
        var _current = _e.scrollTop() / hUnit;
        if(num != _current){
            var intv = setInterval(function(){
                if(_e.scrollTop() / hUnit == 10)_e.scrollTop(0);
                if(_e.scrollTop() / hUnit != num){
                    _e.scrollTop(_e.scrollTop()+1);
                }else{
                    clearInterval(intv);
                }
            },10);
        }
    },
    change:function(e,num,translator){
      e.find('strong').html(translator);
      var _e = e.find('.tranNum');
      for(var i=num.length; i >0 ;i--){
        this.scroll(_e,i-1,parseInt(num.substr(i-1,1)));
      }
    }
};
function renderTranNum(){
    $.get("http://m.yxgapp.com/d/mooc/GetTranslatorAndSentencesNumber.json",{},function(data){
        data=JSON.parse(data);
        //$("div.header strong").html(data.translatorNumber);
        billboard.init($("#tranNum"),data.sentencesNumber.toString(),data.translatorNumber);
    });
}

var initPlayer = function(player){
    /*var el = document.getElementById(player);
    var btn = $(el).next('div');
    el.oncanplay = function(){
        btn.html('<i class="fa fa-play"></i>').click(function(){
            $(el).show().prev('img').hide();
            btn.hide();
            el.play();
        });
    };
    el.onplay = function(){
        btn.fadeOut();
        el.onclick = function(){
            this.pause();
            btn.html('<i class="fa fa-play"></i>').show();
        }
    };
    el.onpause = function(){
        btn.html('<i class="fa fa-play"></i>').show();
    };
    el.onended = function(){
        btn.html('<i class="fa fa-repeat"></i>').show();
    };*/
    $("#box a").click(function(){
      box = $("#box");
      obj = $(this);
      bolA = obj.hasClass("active");
      if(bolA){ //opened
        obj.removeClass("active");
        obj.find("div").html("以下内容有剧透&nbsp;,&nbsp;请注意打开姿势");
        box.animate({"height":"9rem"});
      }else{ // closed
        obj.addClass("active");
        obj.find("div").html("再次隐藏");
        box.animate({"height":parseInt($("#v-content").height())+100+"px"});
      }
    });
    $("#box a").trigger('click');
    $('div.share').click(function(){
        page.showWxShare();
    });
    $(window).resize(function(){
        if(window.orientation==90||window.orientation==-90){
            $('#getApp').hide();
        }else{
            $('#getApp').show();
        }
    });

    if($(window).width()<768){
      $('section.relate li:gt(3)').hide();
    }else{
      $('#player').remove();
      var Player=jwplayer('v-player').setup({
        'flashplayer': 'jwplayer/jwplayer.flash.swf',
        'autostart':'true',
        'width': '100%',
        'height': '27.5rem',
        'image':image,
        'file':file
      });
    }

    if(Player){
      Player.onPlay(function(){
        page.setEvents(page.checkUA(),vid,version,'PlayVideo');
        _hmt.push(['_trackEvent', 'Video', 'Play', 'Play', vid]);
      });
    }else{
      $('video').on('play', function(){
        page.setEvents(page.checkUA(),vid,version,'PlayVideo');
        _hmt.push(['_trackEvent', 'Video', 'Play', 'Play', vid]);
      });
    }
  }



if(page.checkUA() == 'wx'){
    page.getWxSign(window.location.href,function(wxconfig){
        if(wxconfig){
            wxconfig.jsApiList = ["onMenuShareTimeline","onMenuShareAppMessage","onMenuShareQQ","onMenuShareWeibo","onMenuShareQZone"];
            wx.config(wxconfig);
        }
    });
    wx.ready(function(){
        var share = {
            title : document.title,
            desc : document.getElementsByTagName('meta')['description'].content,
            link : window.location.href+'?share=wx',
            imgUrl : "http://playlist.yxgapp.com/static/images/applogo.png",
            success : function () {
                page.setEvents(page.checkUA(),vid,version,'ShareVideo');
                _hmt.push(['_trackEvent', 'Video', 'Share', 'Shared', vid]);
            },
            cancel : function(){
                page.setEvents(page.checkUA(),vid,version,'CancelShareVideo');
                _hmt.push(['_trackEvent', 'Video', 'Share', 'CancelShare', vid]);
            }
        };
        wx.onMenuShareTimeline(share);
        wx.onMenuShareAppMessage(share);
        wx.onMenuShareQQ(share);
        wx.onMenuShareWeibo(share);
        wx.onMenuShareQZone(share);
    });
}

var version = '1.0.0';
initPlayer();
page.setEvents(page.checkUA(),vid,version,'OpenVideo');
